class Busqueda{
constructor(){
this.personas = [
  {nombres : "Marjorie", apellidos:"Conforme", ruta:"Manta-Momtecristi"},
  {nombres : "Carlos", apellidos:"Lopez", ruta:"Manta-SPONDYLUSi"},
  {nombres : "Mishelle", apellidos:"Loor", ruta:"Manta-San Mateo"},
  {nombres : "luis", apellidos:"Ruster", ruta:"Manta-Santa Marianita"},
  {nombres : "Mateo", apellidos:"Ruster", ruta:"Malecon de Manta"},
  {nombres : "Carlos", apellidos:"Arroyo", ruta:"Manta-San Mateo"},
  {nombres : "Marta", apellidos:"Casa", ruta:"Manta-Santa Marianita"},
  {nombres : "Lisbeth", apellidos:"Asucena", ruta:"Malecon de Manta"},
  {nombres : "Tito", apellidos:"Mendoza", ruta:"Manta-Santa Marianita"},
  {nombres : "Karla", apellidos:"Chavez", ruta:"Malecon de Manta"},
  {nombres : "Karen", apellidos:"Ronquillo", ruta:"Malecon de Manta"},
];
this.personasBK = this.personas;
this.OnInit();
console.log(this.personas);
}
OnInit(){
  let cuerpo = document.getElementById("cuerpo");
    while (cuerpo.rows.length > 0) {
      cuerpo.deleteRow(0);

    }
  this.personas.forEach(persona => {
    let fila = cuerpo.insertRow(cuerpo.rows.length);
    fila.insertCell(0).innerHTML= persona.nombres;
    fila.insertCell(1).innerHTML= persona.apellidos;
    fila.insertCell(2).innerHTML= persona.ruta;
  });

}
buscar(id){
  let busqueda = document.getElementById(id).value;
  this.personas = this.personasBK;
  this.personas = this.personas.filter(persona => {
    return persona.ruta.toLowerCase().indexOf(busqueda) > -1;

  });
  this.OnInit();
  }

}
let busqueda = new Busqueda();
let form = document.getElementById("busquedaForm")
form.addEventListener('submit',()=>{
  busqueda.buscar('valor');
});
